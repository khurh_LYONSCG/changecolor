// ==UserScript==
// @name         Color Change
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://*/*
// @grant        none
// @require      https://code.jquery.com/jquery-3.2.1.slim.min.js
// @match        https://*/*
// ==/UserScript==

(function() {
    //'use strict';

    var answer = prompt("Press 1 for custom color\nor\nPress any key for sky blue");

    if(answer === "1") {
        var r = prompt("Enter red(0-255)");
        var g = prompt("Enter green(0-255)");
        var b = prompt("Enter blue(0-255)");
        $("div").css("background-color", "rgb(" + r + "," + g + "," + b + ")");
    } else {
        $("div").css("background-color", "rgb(135, 206, 231)");
    }





})();